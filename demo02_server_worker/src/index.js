const isSupport = document.querySelector('#isSupport')
const isSuccess = document.querySelector('#isSuccess')
const state = document.querySelector('#state')
const swState = document.querySelector('#swState')

window.addEventListener('load', event => {
  if("serviceWorker" in navigator) {
    isSupport.textContent = "支持"
    window.navigator.serviceWorker.register('./server-worker.js', {
      scope: './'
    }).then(registration => {
      var serviceWorker;
      isSuccess.textContent = "注册成功"
      if (registration.installing) {
            serviceWorker = registration.installing;
            state.textContent = "installing";
        } else if (registration.waiting) {
            serviceWorker = registration.waiting;
            state.textContent = 'waiting';
        } else if (registration.active) {
            serviceWorker = registration.active;
            state.textContent = 'active';
        }
        if (serviceWorker) {
            swState.textContent = serviceWorker.state;
            serviceWorker.addEventListener('statechange', function (e) {
               swState.appendChild('&emsp;状态变化为' + e.target.state);
            });
        }
    }).catch(error => {
      isSuccess.textContent = "注册失败"
    })
  } else {
    isSupport.textContent = "不支持"
  }
})